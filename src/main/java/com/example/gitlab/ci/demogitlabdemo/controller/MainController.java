package com.example.gitlab.ci.demogitlabdemo.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/main")
public class MainController {


    @GetMapping(value = "/home")
    public String index(){
        return "Home page";
    }
}
