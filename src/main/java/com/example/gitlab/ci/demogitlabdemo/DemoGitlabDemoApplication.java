package com.example.gitlab.ci.demogitlabdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGitlabDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoGitlabDemoApplication.class, args);
    }

}
